package com.example.uzengdictionary.domain

import android.database.Cursor
import com.example.uzengdictionary.data.source.entity.WordEntity

interface AppRepository {
    fun getAllWords(): Cursor
    fun setFavourite(data: WordEntity)
    fun getEnglishWordsByQuery(query: String): Cursor
    fun getAllFavouriteWords(): Cursor
}