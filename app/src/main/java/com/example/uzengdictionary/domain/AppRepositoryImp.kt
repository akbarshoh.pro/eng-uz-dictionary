package com.example.uzengdictionary.domain

import android.database.Cursor
import com.example.uzengdictionary.data.source.AppDatabase
import com.example.uzengdictionary.data.source.entity.WordEntity

class AppRepositoryImp : AppRepository {
    companion object {
        private var appRepository: AppRepository? = null
        fun getInstance(): AppRepository {
            if (appRepository == null) {
                appRepository = AppRepositoryImp()
            }
            return appRepository!!
        }
    }
    private val wordDao = AppDatabase.getInstance().wordDao()
    override fun getAllWords(): Cursor =
        wordDao.getAllWords()

    override fun setFavourite(data: WordEntity) =
        wordDao.setFavourite(data)

    override fun getEnglishWordsByQuery(query: String): Cursor =
        wordDao.getEnglishWordsByQuery(query)

    override fun getAllFavouriteWords(): Cursor =
        wordDao.getAllFavouriteWords()
}