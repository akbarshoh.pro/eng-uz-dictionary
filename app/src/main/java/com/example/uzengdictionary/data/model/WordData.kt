package com.example.uzengdictionary.data.model

class WordData(
    val english: String,
    val type: String,
    val transcript: String,
    val uzbek: String,
    var isFavourite: Int
)