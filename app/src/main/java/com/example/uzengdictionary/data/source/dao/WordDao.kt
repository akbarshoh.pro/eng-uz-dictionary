package com.example.uzengdictionary.data.source.dao

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import com.example.uzengdictionary.data.source.entity.WordEntity

@Dao
interface WordDao {
    @Query("SELECT * FROM dictionary")
    fun getAllWords(): Cursor

    @Query("SELECT * FROM dictionary WHERE is_favourite = 1")
    fun getAllFavouriteWords(): Cursor

    @Query("SELECT * FROM dictionary WHERE english LIKE :query || '%'")
    fun getEnglishWordsByQuery(query: String): Cursor

    @Update
    fun setFavourite(data: WordEntity)
}