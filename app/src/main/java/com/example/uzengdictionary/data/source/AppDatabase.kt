package com.example.uzengdictionary.data.source

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.uzengdictionary.data.source.dao.WordDao
import com.example.uzengdictionary.data.source.entity.WordEntity

@Database(entities = [WordEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao
    companion object {
        private var instance: AppDatabase? = null

        fun init(context: Context) {
            if (instance == null) {
                instance = Room.databaseBuilder(context, AppDatabase::class.java, "Dictionary.db")
                    .allowMainThreadQueries()
                    .createFromAsset("dictionary.db")
                    .build()
            }
        }

        fun getInstance(): AppDatabase = instance!!
    }
}