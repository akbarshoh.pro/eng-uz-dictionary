package com.example.uzengdictionary.data

import com.example.uzengdictionary.data.model.WordData
import com.example.uzengdictionary.data.source.entity.WordEntity

object MyMapper {
    fun WordEntity.toData() : WordData =
        WordData(english, type, transcript, uzbek, isFavourite)
}