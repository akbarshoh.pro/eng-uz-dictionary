package com.example.uzengdictionary.data.source.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dictionary", )
class WordEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val english: String,
    val type: String,
    val transcript: String,
    val uzbek: String,
    val countable: String,
    @ColumnInfo("is_favourite")
    var isFavourite: Int
)