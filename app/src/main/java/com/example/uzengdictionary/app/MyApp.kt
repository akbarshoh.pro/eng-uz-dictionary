package com.example.uzengdictionary.app

import android.app.Application
import com.example.uzengdictionary.data.source.AppDatabase

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        AppDatabase.init(this)
    }
}