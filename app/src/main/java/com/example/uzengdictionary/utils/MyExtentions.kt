package com.example.dictionaryapp.utils

import android.graphics.Color
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.fragment.app.FragmentActivity

fun FragmentActivity.popBackStack() {
    supportFragmentManager.popBackStack()
}

fun String.createSpannable(query: String): SpannableString {
    val spannable = SpannableString(this)
    val startPos = this.indexOf(query)
    val endPos = startPos + query.length
    if (startPos < 0 || endPos > this.length)
        return spannable
    spannable.setSpan(
        ForegroundColorSpan(Color.parseColor("#E6E7505B")),
        startPos,
        endPos,
        SpannableString.SPAN_EXCLUSIVE_INCLUSIVE,
    )

    return spannable
}