package com.example.uzengdictionary.presentation.screen.fav

import com.example.uzengdictionary.data.source.entity.WordEntity

class FavPresenter(private val view: FavContract.View) : FavContract.Presenter {
    private val model: FavContract.Model = FavModel()

    override fun loadWords() {
        val cursor = model.getAllFavouriteWords()
        view.isEmpty(cursor.count == 0)
        view.showWords(cursor)
    }

    override fun setFavourite(data: WordEntity) =
        model.setFavourite(data)
}