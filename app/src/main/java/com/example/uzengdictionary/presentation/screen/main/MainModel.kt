package com.example.uzengdictionary.presentation.screen.main

import android.database.Cursor
import com.example.uzengdictionary.data.source.entity.WordEntity
import com.example.uzengdictionary.domain.AppRepository
import com.example.uzengdictionary.domain.AppRepositoryImp

class MainModel : MainContract.Model {
    private val appRepository: AppRepository = AppRepositoryImp.getInstance()

    override fun getAllWords(): Cursor =
        appRepository.getAllWords()

    override fun getEnglishWords(query: String): Cursor =
        appRepository.getEnglishWordsByQuery(query)

    override fun setFavourite(data: WordEntity) =
        appRepository.setFavourite(data)

}