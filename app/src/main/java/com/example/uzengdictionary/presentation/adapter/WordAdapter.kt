package com.example.uzengdictionary.presentation.adapter

import android.annotation.SuppressLint
import android.database.Cursor
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.dictionaryapp.utils.createSpannable
import com.example.uzengdictionary.R
import com.example.uzengdictionary.data.MyMapper.toData
import com.example.uzengdictionary.data.model.WordData
import com.example.uzengdictionary.data.source.entity.WordEntity
import com.example.uzengdictionary.databinding.ItemBinding

class WordAdapter : RecyclerView.Adapter<WordAdapter.VH>() {
    private var _cursor: Cursor? = null
    private var listener: ((WordEntity, String) -> Unit)? = null
    private var openDialog: ((WordEntity) -> Unit)? = null
    private val cursor: Cursor get() = _cursor!!
    private var query: String = ""
    @SuppressLint("Range")
    inner class VH(private val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                cursor.moveToPosition(absoluteAdapterPosition)
                val data = WordEntity(
                    id = cursor.getLong(cursor.getColumnIndex("id")),
                    english = cursor.getString(cursor.getColumnIndex("english")),
                    type = cursor.getString(cursor.getColumnIndex("type")),
                    transcript = cursor.getString(cursor.getColumnIndex("transcript")),
                    uzbek = cursor.getString(cursor.getColumnIndex("uzbek")),
                    countable = cursor.getString(cursor.getColumnIndex("countable")),
                    isFavourite = cursor.getInt(cursor.getColumnIndex("is_favourite"))
                )
                openDialog?.invoke(data)
            }

            binding.imgLike.setOnClickListener {
                cursor.moveToPosition(absoluteAdapterPosition)
                val data = WordEntity(
                    id = cursor.getLong(cursor.getColumnIndex("id")),
                    english = cursor.getString(cursor.getColumnIndex("english")),
                    type = cursor.getString(cursor.getColumnIndex("type")),
                    transcript = cursor.getString(cursor.getColumnIndex("transcript")),
                    uzbek = cursor.getString(cursor.getColumnIndex("uzbek")),
                    countable = cursor.getString(cursor.getColumnIndex("countable")),
                    isFavourite = if (cursor.getInt(cursor.getColumnIndex("is_favourite")) == 0) 1 else 0
                )
                listener?.invoke(data, query)
            }
        }
        @SuppressLint("SetTextI18n")
        fun bind(entity: WordEntity) {
            val data = entity.toData()
            binding.textWord.text = data.english.createSpannable(query!!)
            if (data.isFavourite == 0) binding.imgLike.setImageResource(R.drawable.no_like)
            else binding.imgLike.setImageResource(R.drawable.like)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = cursor.count

    @SuppressLint("Range")
    override fun onBindViewHolder(holder: VH, position: Int) {
        cursor.moveToPosition(position)
        val data = WordEntity(
            id = cursor.getLong(cursor.getColumnIndex("id")),
            english = cursor.getString(cursor.getColumnIndex("english")),
            type = cursor.getString(cursor.getColumnIndex("type")),
            transcript = cursor.getString(cursor.getColumnIndex("transcript")),
            uzbek = cursor.getString(cursor.getColumnIndex("uzbek")),
            countable = cursor.getString(cursor.getColumnIndex("countable")),
            isFavourite = cursor.getInt(cursor.getColumnIndex("is_favourite"))
        )
        holder.bind(data)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setCursor(cursor: Cursor, query: String) {
        this._cursor?.close()
        this._cursor = cursor
        this.query = query
        notifyDataSetChanged()
    }

    fun dataTransfer(block: (WordEntity, String) -> Unit) {
        listener = block
    }

    fun openDialog(block: (WordEntity) -> Unit) {
        openDialog = block
    }

}