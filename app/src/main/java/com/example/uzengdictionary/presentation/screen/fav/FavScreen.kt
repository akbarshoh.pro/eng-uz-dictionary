package com.example.uzengdictionary.presentation.screen.fav

import android.database.Cursor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.uzengdictionary.databinding.ScreenFavouriteBinding
import com.example.uzengdictionary.presentation.adapter.WordAdapter

class FavScreen : Fragment(), FavContract.View {
    private var _binding: ScreenFavouriteBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: WordAdapter
    private val presenter: FavContract.Presenter by lazy { FavPresenter(this) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenFavouriteBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }

        adapter = WordAdapter()
        binding.recyclerFav.adapter = adapter
        binding.recyclerFav.layoutManager = LinearLayoutManager(requireContext())
        adapter.dataTransfer { data, _ ->
            presenter.setFavourite(data)
            presenter.loadWords()
        }

        presenter.loadWords()
    }


    override fun showWords(cursor: Cursor) {
        adapter.setCursor(cursor, "")
    }

    override fun isEmpty(bool: Boolean) {
        binding.noFav.isVisible = bool
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}