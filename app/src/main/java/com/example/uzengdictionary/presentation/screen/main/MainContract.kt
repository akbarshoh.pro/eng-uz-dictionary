package com.example.uzengdictionary.presentation.screen.main

import android.database.Cursor
import com.example.uzengdictionary.data.source.entity.WordEntity

interface MainContract {
    interface Model {
        fun getAllWords(): Cursor
        fun getEnglishWords(query: String): Cursor
        fun setFavourite(data: WordEntity)
    }

    interface View {
        fun showAllWOrds(cursor: Cursor, query: String)
    }

    interface Presenter {
        fun loadWords()
        fun loadEnglishWords(query: String)
        fun setFavourite(data: WordEntity)
    }
}