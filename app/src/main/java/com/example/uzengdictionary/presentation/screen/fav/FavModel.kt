package com.example.uzengdictionary.presentation.screen.fav

import android.database.Cursor
import com.example.uzengdictionary.data.source.AppDatabase
import com.example.uzengdictionary.data.source.entity.WordEntity

class FavModel : FavContract.Model {
    private val wordDao = AppDatabase.getInstance().wordDao()

    override fun getAllFavouriteWords(): Cursor =
        wordDao.getAllFavouriteWords()

    override fun setFavourite(data: WordEntity) =
        wordDao.setFavourite(data)

}