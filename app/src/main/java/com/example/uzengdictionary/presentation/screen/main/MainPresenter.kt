package com.example.uzengdictionary.presentation.screen.main

import com.example.uzengdictionary.data.source.entity.WordEntity

class MainPresenter(private val view: MainContract.View) : MainContract.Presenter {
    private val model: MainContract.Model = MainModel()
    override fun loadWords() {
        val cursor = model.getAllWords()
        view.showAllWOrds(cursor, "")
    }

    override fun loadEnglishWords(query: String) {
        val cursor = model.getEnglishWords(query)
        view.showAllWOrds(cursor, query)
    }

    override fun setFavourite(data: WordEntity) =
        model.setFavourite(data)

}