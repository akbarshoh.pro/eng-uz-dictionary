package com.example.uzengdictionary.presentation.screen.main

import android.app.Dialog
import android.content.Context
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.uzengdictionary.R
import com.example.uzengdictionary.data.source.entity.WordEntity
import com.example.uzengdictionary.databinding.ScreenMainBinding
import com.example.uzengdictionary.presentation.adapter.WordAdapter
import java.util.Locale

class MainScreen : Fragment(), MainContract.View, TextToSpeech.OnInitListener {
    private var _binding: ScreenMainBinding? = null
    private val binding get() = _binding!!
    private var time = System.currentTimeMillis()
    private val presenter: MainContract.Presenter by lazy { MainPresenter(this) }
    private lateinit var adapter: WordAdapter
    private lateinit var cursor: Cursor
    private var currentQuery: String? = null
    private var tts: TextToSpeech? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tts = TextToSpeech(requireContext(), this)
        
        binding.btnFavourite.setOnClickListener {
            if (System.currentTimeMillis() - time > 500) {
                findNavController().navigate(MainScreenDirections.actionMainScreenToFavScreen())
                time = System.currentTimeMillis()
            }
        }

        adapter = WordAdapter()
        binding.rvDictionary.adapter = adapter
        binding.rvDictionary.layoutManager = LinearLayoutManager(requireContext())

        adapter.openDialog { showDialog(it, requireContext()) }

        adapter.dataTransfer { data, query ->
            presenter.setFavourite(data)
            presenter.loadEnglishWords(query)
        }
        search()
        presenter.loadWords()
    }


    private fun showDialog(item: WordEntity, context: Context) {
        val dialog = Dialog(context)

        dialog.setContentView(R.layout.custom_word_dialog)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val word: AppCompatTextView = dialog.findViewById(R.id.txtWord)
        val type: AppCompatTextView = dialog.findViewById(R.id.txtType)
        val transcript: AppCompatTextView = dialog.findViewById(R.id.txtTranscript)
        val translate: AppCompatTextView = dialog.findViewById(R.id.txtTrans)

        val audio: AppCompatImageView = dialog.findViewById(R.id.btnVolume)

        item.apply {
            word.text = this.english
            type.text = this.type
            transcript.text = this.transcript
            translate.text = this.uzbek
        }

        audio.setOnClickListener {
            tts?.speak(item.english, TextToSpeech.QUEUE_FLUSH, null, "")
        }

        dialog.create()
        dialog.show()
        dialog.setCancelable(true)
    }


    private fun search() {
        binding.inputSearch.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    currentQuery = newText
                    if (currentQuery == null) presenter.loadWords()
                    else  presenter.loadEnglishWords(currentQuery!!)

                    return true
                }
            })
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                //
            }
        }
    }

    override fun showAllWOrds(cursor: Cursor, query: String) {
        this.cursor = cursor
        adapter.setCursor(cursor, query)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }
    }

}