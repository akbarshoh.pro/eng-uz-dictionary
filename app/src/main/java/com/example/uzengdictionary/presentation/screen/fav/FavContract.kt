package com.example.uzengdictionary.presentation.screen.fav

import android.database.Cursor
import com.example.uzengdictionary.data.source.entity.WordEntity

interface FavContract {
    interface Model {
        fun getAllFavouriteWords(): Cursor
        fun setFavourite(data: WordEntity)
    }
    interface View {
        fun showWords(cursor: Cursor)
        fun isEmpty(bool: Boolean)
    }
    interface Presenter {
        fun loadWords()
        fun setFavourite(data: WordEntity)
    }
}